//jshint strict: false
module.exports = function(config) {
  config.set({    basePath: './app',    files: [
    'bower_components/angular/angular.js',
    'bower_components/angular-route/angular-route.js',
    'bower_components/angular-mocks/angular-mocks.js',
    'bower_components/angular/angular.js',
    'bower_components/angular-ui-router/release/angular-ui-router.min.js',
    'bower_components/jquery/dist/jquery.js',
    'bower_components/bootstrap/dist/js/bootstrap.js',
    'bower_components/firebase/firebase.js',
    'bower_components/angular-i18n/angular-locale_es-mx.js',
    'bower_components/angular-bootstrap/ui-bootstrap.js',
    'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
    'bower_components/angularfire/dist/angularfire.js',
    'bower_components/ng-parallax/angular-parallax.js',
    'bower_components/angular-scroll/angular-scroll.js',
    'bower_components/parallax.js/parallax.min.js',
    'bower_components/angular-socialshare/dist/angular-socialshare.js',
    'bower_components/angular-parallax/scripts/angular-parallax.js',
    'components/main/main.js',
    'components/register/register.js',
    'components/test/register/test.js',
    'components/test/register/register.controller.spec.js',
    'components/test/register/register.service.spec.js',
    'components/test/main/main.controller.spec.js',
    'components/**/*.js'
  ],    autoWatch: true,    frameworks: ['jasmine'],    browsers: ['PhantomJS'],    plugins: [
    'karma-chrome-launcher',
    'karma-firefox-launcher',
    'karma-phantomjs-launcher',
    'karma-jasmine',
    'karma-junit-reporter'
  ],    junitReporter: {
    outputFile: 'test_out/unit.xml',
    suite: 'unit'
  }  });
};