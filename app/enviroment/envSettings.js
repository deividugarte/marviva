(function() {
    'use strict';

    angular
        .module('marvivaApp')
        .constant('env', {
            animateFadeInLeft: 'fadeInLeft',
            animateFadeInDown: 'fadeInDown',
            animatePulse: 'pulse',
            animateFadeInRight: 'fadeInRight',
            formatDate: 'yyyy-MM-dd',
            filterName: 'date',
            firstSection: '1',
            tagUser: 'userName',
            finalView: 'final',
            mainView: 'main',
            notVisible: 'not-visible',
            animated: 'animated ',
            infinitePulse: 'infinite pulse'
        });
})();