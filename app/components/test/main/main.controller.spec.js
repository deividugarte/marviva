(function() {
       describe('MainCtrl', function() {
        var $this;
        var MainSrv;
        var $state;
        var $filter;
        var $controller;
        var $q;
        beforeEach(module('marvivaApp.common'));
        beforeEach(module('marvivaApp.main'));
        beforeEach(module(function ($provide) {
            $provide.service('MainSrv', function () {
                this.getMessages = function getMessages() {};
            });
        }));
        beforeEach(inject(function(_$controller_, _$q_, _MainSrv_, _$state_, _$filter_) {
            $controller = _$controller_;
            $q = _$q_;
            MainSrv = _MainSrv_;
            $state = _$state_;
            $filter = _$filter_;

        }));
        beforeEach(function () {
            $this = $controller('MainCtrl', {
                MainSrv: MainSrv,
                $state: $state,
                $filter: $filter

                });
            });
            it("Should be initialized", function () {
                 expect($this).toBeDefined();
            });
            it("Should be parsed", function () {
                expect($this.parseDate("2014-03-02")).toEqual(new Date(2014, 02, 02));
            });
            it("Should be minutes", function () {
                expect($this.diffMinutes(new Date(2016,06,11), new Date(2016, 06, 12))).toBe(1440);
            });
            it("Should be hours", function () {
                expect($this.diffHours(new Date(2016,06,11), new Date(2016, 06, 10))).toBe(24);
            });
            it("Should be Days", function () {
                expect($this.diffDays(new Date(2016,06,11), new Date(2016, 06, 10))).toBe(1);
            });
            it("should be the number of Plastic", function (){
                expect($this.calculateCountPlastic(5)).toBe(40);
            });
            it("should not be the number of Plastic", function (){
                expect($this.calculateCountPlastic("q")).toBeNaN();
            });
            it("should be the number of garbage trucks", function(){
                expect($this.calculateCountBottles(10)).toBe(1500);
            });
            it("should not be the number of garbage trucks", function(){
                expect($this.calculateCountBottles("q")).toBeNaN();
            });
            it("should be the age", function () {
                expect($this.calculateAge(628668000000)).toBe(26);
            });
           it("should not be the age", function () {
               expect($this.calculateAge("q")).toBeNaN();
           });
           it("should be the first message", function (){
               $state.params.name  = "Daniel";
               var message = "Actualmente cada año 8 millones de toneladas de plástico " +
                   "terminan en los océanos del mundo.Es decir Daniel, " +
                   "a lo largo de tu vida, alrededor de " + 20 + " millones de " +
                   "toneladasde plástico han ido a parar a los océanos." +
                   "Esos 8 millones de toneladas de plástico que se " +
                   "depositan cada año es lo mismo a que el contenido " +
                   "equivalente al que está dentro de un camión de " +
                   "basura se tire a los océanos ¡cada minuto!" +
                   "Es decir, en este año que has vivido, " +
                   "hasta ayer que han pasado " + 3 + " días " +
                   "lo mismo que " + 3 + " horas o " + 3 +
                   " minutos, lo equivalente a " + 3 + " camiones " +
                   "de basura llenos de plástico ha ido a parar a los océanos. \n" +
                   "Si seguimos el nivel de producción. " +
                   "Para el 2030, serán 2 camiones de basura " +
                   "cada minuto y para el 2050 serán 4 camiones " +
                   "de basura por minuto.\n" +
                   "Con este nivel, en el 2050 habrá más plástico que peces en los océanos.";
               expect($this.getFirstMessage(3, 3, 3, 20)).toEqual(message);
           });
           it("should be the second message", function (){
               var message = "Nuestro país no escapa. En Costa Rica, cada día al menos " +
                   "120 toneladas métricas de desechos plásticos se depositan en el ambiente, " +
                   "una parte de eso queda en bosques, ríos y océanos. Esto es el equivalente " +
                   "al contenido de 13 camiones de basura llenos de plástico. Es decir. " +
                   "Desde que vos gritaste y lloraste ¡FELIZ AÑO NUEVO! Hace más de " +
                   "cinco meses, lo equivalente a " + 15 + " camiones de basura llenos " +
                   "de plástico, se han quedado en el ambiente.";
               expect($this.getSecondMessage(15)).toEqual(message);
           });
           it("should be the third message", function (){
               $state.params.name  = "Daniel";
               var message = "Tenés que entender " + "Daniel" + " que los plásticos " +
                   "no se degradan, sino que se dividen en pequeñas partículas." +
                   "Te dirán que son oxodegradable o bioplásticos, pero es publicidad. " +
                   "Expertos advierten que eso es falso y el plástico nunca se integran " +
                   "al ambiente de ninguna forma.";
               expect($this.getThirdMessage()).toEqual(message);
           });
           it("should be the fourth message", function (){
               var message = "4 mensaje";
               expect($this.getFourthMessage()).toEqual(message);
           });
           it("should be the fifth message", function (){
               var message = "Encima no la manejamos muy bien. En Costa Rica menos del 2% " +
                   "de los residuos que se generan logran ser recolectados de " +
                   "manera diferenciada. Es decir, desde que tenés cédula, de " +
                   "esos 510 kilos de plásticos que consumiste, poco más de " +
                   + 10 + " kilos fueron recolectados de forma diferenciada y " +
                   "mucho menos de eso, reciclado.";
               expect($this.getFifthMessage(10)).toEqual(message);
           });
           it("should be the sixth message", function (){
               $state.params.name  = "Daniel";
               var message = "Cuando terminas la rutina del gym o " +
                   "luego de las mejengas ¿Qué debemos hacer?" +
                   "¡Claro! Debemos tomar agua, la hidratación " +
                   "es importante para nuestra salud,  pero nuestro " +
                   "hábito tiene un impacto. En Costa Rica cada año – vos " +
                   "Daniel" + " - has utilizado y desechado 150 botellas de " +
                   "plástico, desde que dejaste el chupón has consumido(tiene " + 1 + " años) " +
                   10 + " botellas de plástico desechable.";
               expect($this.getSixthMessage(1, 10)).toEqual(message);
           });
           it("should be the seventh message", function (){
               var message = "Además de lo que ya producimos acá, " +
                   "por año se importa un mínimo de 427 mil toneladas " +
                   "métricas de plástico. Esto equivale al contenido " +
                   "de 53 mil camiones recolectores de basura. " +
                   "Es decir, desde que Costa Rica fue la sensación en Brasil," +
                   " lo equivalente a 106 mil camiones con plásticos han " +
                   "ingresado a nuestras fronteras, para su uso.";
               expect($this.getSeventhMessage()).toEqual(message);
           });
           it("should be the eight message", function (){
               var message = "Ahora, calculá que el 80% de contaminación marina " +
                   "(por todo tipo de residuos) proviene de tierra firma. " +
                   "Es decir, es lo que generamos lejos de la costa lo que " +
                   "está matando a nuestros mares. " +
                   "Y de todos esos desechos flotando, más del 90%  " +
                   "es – ¿adiviná? ¡si lo adivinaste! – ¡PLÁSTICO!";
               expect($this.getEightMessage()).toEqual(message);
           });
           it("should be the nineth message", function (){
               var message = "Entonces ¿qué hacemos? Tenemos que " +
                   "cambiar los modelos de producción y " +
                   "el mercado del plástico. Necesitamos nueva regulación. " +
                   "Y sobre todo, vos y yo tenemos que cambiar nuestros hábitos. " +
                   "Usá bolsa de tela en lugar de estar llevando tus compras en bolsas plásticas. " +
                   "Dejá de comprar botellas plásticas, " +
                   "andá una de vidrio que podés llenar de agua cuando querás. " +
                   "Hacé frescos en casa y lo tomás desde una botella de vidrio. " +
                   "Y pedí en cualquier comercio que NO te traigan una pajilla en tu bebida, " +
                   "sea un batido de fresa, un fresco de tamarindo o una cerveza. ¡NO LA NECESITÁS!";
               expect($this.getNinethMessage()).toEqual(message);
           });
       });
})();

