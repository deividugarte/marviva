describe('calculator', function () {

    beforeEach(module('calculatorApp'));

    describe('sum', function () {
        it('1 + 1 should equal 2', function () {
            expect(1).toBe(1);
        });
    });
});