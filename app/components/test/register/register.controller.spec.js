(function() {
    describe('RegisterCtrl', function() {
        var $this;
        var RegisterSrv;
        var $state;
        var $filter;
        var $controller;
        var $q;
        beforeEach(module('marvivaApp.common'));
        beforeEach(module('marvivaApp.register'));
        beforeEach(module(function ($provide) {
            $provide.service('RegisterSrv', function () {
                this.register = function (data) {};
            });
        }));
        beforeEach(inject(function (_$controller_ , _$q_ ,_RegisterSrv_, _$state_, _$filter_) {
            $controller = _$controller_;
            $q = _$q_;
            RegisterSrv = _RegisterSrv_;
            $state = _$state_;
            $filter = _$filter_;
        }));

        beforeEach(function () {
            $this = $controller('RegisterCtrl', {
                RegisterSrv: RegisterSrv,
                $state: $state,
                $filter: $filter
            });
        });

        it("Should be initialized", function () {
            expect($this).toBeDefined();
        });

        it("Should be insert new client", function () {
            var newUser = {
                name: 'Daniel',
                lastName: 'Peralta',
                date: new Date()
            };
            var deferredSuccess = $q.defer();
            spyOn(RegisterSrv, 'register').and.returnValue(deferredSuccess.promise);
            $state.go = function (newState, opts) {

            };
            $this.start();
            expect(RegisterSrv.register).toHaveBeenCalled();
            //expect(RegisterSrv.register).toHaveBeenCalledWith(newUser);
            deferredSuccess.resolve();
        });

    });
})();
