(function(){
  'use strict'
  angular
    .module('marvivaApp.final')
    .config(setupRoutes);


  setupRoutes.$inject = ['$stateProvider'];
  function setupRoutes($stateProvider){
    $stateProvider
      .state('final', {
        templateUrl: 'components/final/view.html',
        controller: 'FinalCtrl as vm'
      });
  }

})();
