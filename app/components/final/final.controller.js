(function(){
  'use strict'
  
  angular
    .module('marvivaApp.final')
    .controller('FinalCtrl',FinalCtrl);

  /* @ngInject */
  function FinalCtrl($window) {
    var vm = this;
    vm.advices = [
      "1. Pensá primero: antes de comprar algo hecho de plástico, preguntate si existe otra alternativa, como cartón, aluminio o vidrio.",
      "2. Evitá las bolsas plásticas cuando vayás de compras. Llevá siempre bolsas de tela.",
      "3. Pedí siempre tus bebidas SIN pajilla. ¡No la necesitás!",
      "4. Evitá comprar agua en botellas de plástico desechable. Usá una botella reutilizable (vidrio o aluminio) y llenala del tubo.",
      "5. Hacé frescos o jugos en casa y tomalos en botellas reutilizables.",
      "6. Comprá productos empaquetados en cartón o vidrio.",
      "7. En fiestas o reuniones, no usés platos, vasos ni utensilios de plástico desechable. Comprá de cartón o usá la vajilla de tu casa ¡Solo es lavar un poco más!",
      "8. Cambiá los recipientes para alimentos de plástico, por los de vidrio o acero inoxidable.",
      "9. Si vas a comprar comida en la soda cercana al trabajo, llevá tu propio recipiente. Evitá los tradicionales recipientes de estereofón y plástico desechable.",
      "10. Tratá de reutilizar al máximo las botellas de plástico, que inevitablemente se consumen para limpieza de la casa y otros. "
      ];

    vm.beginOnTopWindow = beginOnTopWindow;
    vm.animateElementIn = animateElementIn;
    vm.animateElementOut = animateElementOut;

    beginOnTopWindow();

    function beginOnTopWindow()
    {
      window.scrollTo(0, 0);
    }

    function animateElementIn($el){
      $el.removeClass('not-visible');
      $el.addClass('animated fadeInDown');
    }

    function animateElementOut ($el){
      $el.addClass('not-visible');
      $el.removeClass('animated fadeInDown');
    }
  }

})();
