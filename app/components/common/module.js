(function(){
  'use strict'
  
  angular
  	.module('marvivaApp.common',[
    'ui.router',
    'firebase',
    'ui.bootstrap',
    'angular-scroll-animate',
    'ngCookies'
  ]);
  	
})();
