(function(){
  'use strict'
  
  angular
    .module('marvivaApp.main')
    .controller('MainCtrl',MainCtrl);

  /* @ngInject */
  function MainCtrl(MainSrv, $state, env, $interval, $filter, $window) {
    var vm = this;
    var intervalDays = null;
    var intervalHours = null;
    var intervalMinutes = null;
    var intervalTrashes = null;
    var countTrashTrucks = 0;
    vm.userName = "";
    vm.sectionNumber = 0;
    vm.countPlastic = 0;
    vm.countDownDay = 0;
    vm.countDownHour = 0;
    vm.countDownMinute = 0;
    vm.countDownTrash = 0;
    vm.firstMessage = "";
    vm.fourthMessage = "";
    vm.fifthMessageFirst = "";
    vm.fifthMessageSecond = "";
    vm.sixthMessage = "";
    vm.seventhMessageFirst = "";
    vm.seventhMessageSecond = "";
    vm.eighthMessageFirst = "";
    vm.eighthMessagesSecond = "";
    vm.ninethMessageFirst = "";
    vm.ninethMessageSecond = "";
    vm.viewAdvices= viewAdvices;
    vm.beginOnTopWindow = beginOnTopWindow;
    vm.eventOnCountDownDays = eventOnCountDownDays;
    vm.eventOnCountDownHours = eventOnCountDownHours;
    vm.eventOnCountDownMinutes = eventOnCountDownMinutes;
    vm.eventOnCountDownTrashes = eventOnCountDownTrashes;
    vm.getSectionNumberBySectionId = getSectionNumberBySectionId;
    vm.getCountsDownForThirdMessage = getCountsDownForThirdMessage;
    vm.activate = activate;
    vm.animateFadeInLeftElementIn = animateFadeInLeftElementIn;
    vm.animateFadeInLeftElementOut = animateFadeInLeftElementOut;
    vm.animateFadeInDownElementIn = animateFadeInDownElementIn;
    vm.animateFadeInDownElementOut = animateFadeInDownElementOut;
    vm.animatePulseElementIn = animatePulseElementIn;
    vm.animatePulseElementOut = animatePulseElementOut;
    vm.animateFadeInRightElementIn = animateFadeInRightElementIn;
    vm.animateFadeInRightElementOut = animateFadeInRightElementOut;
    vm.animateFadeInRightForSecondMessageElementIn = animateFadeInRightForSecondMessageElementIn;
    vm.animateFadeInLeftForThirdMessageElementIn = animateFadeInLeftForThirdMessageElementIn;
    vm.animateFadeInLeftForFifthMessageElementIn = animateFadeInLeftForFifthMessageElementIn;
    vm.loadMessages = loadMessages;

    activate();

    function viewAdvices(){
      $state.go(env.finalView);
    }

    function activate(){
      setBirthDate($state.params.date);
      setUser($state.params.name);
      beginOnTopWindow();
      setMessages();
      loadMessages();
    }

    function loadMessages() {
      vm.firstMessage = "¡CADA AÑO!, Actualmente 8 millones de toneladas de plástico van directamente al OCEANO.";
      vm.fourthMessage = vm.userName + ", A lo largo de tu vida, has utilizado alrededor de " + vm.countPlastic + " millones de toneladas de plástico";
      vm.fifthMessageFirst = "Hasta el día de hoy has vivido " + vm.countDownDay + " días, " + vm.countDownHour + " horas.";
      vm.fifthMessageSecond =  "Lo equivalente a " + vm.countDownTrash + " camiones de basura llenos de plástico desechable porque cada minuto que osa, va a parar al ambiente.";
      vm.sixthMessage = "SI SEGUIMOS ASÍ... Para el 2030, serán 2 camiones de basura por minuto, Para el 2050, serán 4 camiones de basura por minuto";
      vm.seventhMessageFirst = "Cada día, 120 toneladas de plástico se depositan en el MEDIO AMBIENTE.";
      vm.seventhMessageSecond = "Lo que es equivalente cada año a 4745 camiones de basura. ¡LLENOS DE PLÁSTICO!";
      vm.eighthMessageFirst = vm.userName + ", los plásticos NO SE DEGRADAN";
      vm.eighthMessageSecond = "Te dirán son oxodegradantes o bioplásticos pero, ES PURA PUBLICIDAD, Expertos advierten que es FALSO";
      vm.ninethMessageFirst = "Cada día, en promedio en Costa Rica cada persona produce entre 1 y 2 kg de residuos.";
      vm.ninethMessageSecond = "17% son residuos de algún tipo de plástico.";
    }

    function setMessages(){
      vm.userName = getUser();
      vm.sectionNumber = getSectionNumberBySectionId(env.firstSection);
      vm.countPlastic = getCountOfPlasticForSecondMessage();
      countTrashTrucks = getCountOfTrashTrucksForFifthMessage();
      getCountsDownForThirdMessage();
    }

    function beginOnTopWindow(){
      window.scrollTo(0, 0);
    }

    function animateFadeInLeftElementIn($message){
      $message.removeClass(env.notVisible);
      $message.addClass(env.animated + env.animateFadeInLeft);
    }

    function animateFadeInLeftElementOut($message){
      $message.addClass(env.notVisible);
      $message.removeClass(env.animated + env.animateFadeInLeft);
    }

    function animateFadeInDownElementIn($message){
      $message.removeClass(env.notVisible);
      $message.addClass(env.animated + env.animateFadeInDown);
    }

    function animateFadeInDownElementOut($message){
      $message.addClass(env.notVisible);
      $message.removeClass(env.animated + env.animateFadeInDown);
    }

    function animatePulseElementIn($message){
      $message.removeClass(env.notVisible);
      $message.addClass(env.animated + env.infinitePulse);
    }

    function animatePulseElementOut($message){
      $message.addClass(env.notVisible);
      $message.removeClass(env.animated + env.infinitePulse);
    }

    function animateFadeInRightElementIn($message){
      $message.removeClass(env.notVisible);
      $message.addClass(env.animated + env.animateFadeInRight);
    }

    function animateFadeInRightElementOut($message){
      $message.addClass(env.notVisible);
      $message.removeClass(env.animated + env.animateFadeInRight);
    }

    function animateFadeInRightForSecondMessageElementIn($message){
      $message.removeClass(env.notVisible);
      $message.addClass(env.animated + env.animateFadeInRight);
    }

    function animateFadeInLeftForThirdMessageElementIn($message){
      $message.removeClass(env.notVisible);
      $message.addClass(env.animated + env.animateFadeInLeft);
    }

    function animateFadeInLeftForFifthMessageElementIn($message) {
      $message.removeClass(env.notVisible);
      $message.addClass(env.animated + env.animateFadeInLeft);
    }

    function eventOnCountDownDays(){
      vm.countDownDay = setCurrentCountDownDay(intervalDays);
      loadMessages();
    }

    function eventOnCountDownHours(){
      vm.countDownHour = setCurrentCountDownHour(intervalHours);
      loadMessages();
    }

    function eventOnCountDownMinutes(){
      vm.countDownMinute = setCurrentCountDownMinute(intervalMinutes);
      loadMessages();
    }

    function eventOnCountDownTrashes(){
      vm.countDownTrash = setCurrentCountDownTrash(intervalTrashes);
      loadMessages()
    }

    function getSectionNumberBySectionId(sectionId){
      vm.sectionNumber = "Cantidad de mensajes: " + sectionId + "-8";
    }

    function countDownDays(nDays){
      intervalDays = $interval(function(){
          vm.countDownDay++;
      },1,nDays);
    }

    function countDownHours(hours){
      intervalHours = $interval(function(){
          vm.countDownHour++;
      },1,hours);
    }

    function countDownMinutes(minutes){
      intervalMinutes = $interval(function(){
          vm.countDownMinute++;
      },1,minutes);
    }

    function countDownTrashes(trashes){
      intervalTrashes = $interval(function(){
          vm.countDownTrash++;
      },1,trashes);
    }

    function getCountOfTrashTrucksForFifthMessage(){
      return calculateCountGarbageTrucks(getDiffDays());
    }

    function getDiffDays(){
      var nDays = diffDays(parseDate(getCurrentDate()), parseDate(getBirthDate()));
      return nDays;
    }

    function getDiffHours(){
      var hours = diffHours(parseDate(getCurrentDate()), parseDate(getBirthDate()));
      return hours;
    }

    function getDiffMinutes(){
      var minutes = diffMinutes(parseDate(getCurrentDate()),parseDate(getBirthDate()));
      return minutes;
    }

    function getCountTruckTrashes(){
      vm.countDownTrash = diffHours(parseDate(getCurrentDate()),parseDate(getBirthDate())) * 24;
      return vm.countDownTrash;
    }

    function getCurrentDate(){
      vm.currentDate = $filter(env.filterName)(Date.now(), env.formatDate);
      return vm.currentDate;
    }

    function getInitDate(){
      var date = new Date();
      date = new Date(date.getFullYear(),0,1);
      vm.initDate = $filter(env.filterName)(date, env.formatDate);
      return vm.initDate;
    }

    function getBirthDate(){
      vm.dateOfBirth = $filter(env.filterName)(localStorage.getItem(env.filterName), env.formatDate);
      return vm.dateOfBirth;
    }

    function getCountsDownForThirdMessage() {
      countDownDays(getDiffDays());
      countDownHours(getDiffHours());
      countDownMinutes(getDiffMinutes());
      countDownTrashes(getCountTruckTrashes());
      loadMessages();
    }

    function resetCountsDown(){
      vm.countDownDay = 0;
      vm.countDownHour = 0;
      vm.countDownMinute = 0;
      vm.countDownTrash = 0;
    }

    function parseDate(date){
      var split = date.split('-');
      date = new Date(split[0], split[1] - 1, split[2]);
      return date;
    }

    function diffMinutes(currentDate, initDate){
      var minutes = Math.abs(currentDate - initDate) / 60000;
      return minutes;
    }

    function diffHours(currentDate, initDate){
      var hours = Math.abs(currentDate - initDate) / 3600000;
      return hours;
    }

    function diffDays(currentDate, initDate) {
      var transformCurrentDate = currentDate;
      var transformInitDate = initDate;
      var nDays = transformCurrentDate-transformInitDate;
      nDays = Math.floor(nDays / (1000 * 60 * 60 * 24));
      return nDays;
    }

    function calculateCountPlastic(age){
      vm.countPlastic = age * 8;
      return vm.countPlastic;
    }

    function calculateCountGarbageTrucks(nDays){
      countTrashTrucks = nDays * 13;
      return countTrashTrucks;
    }

    function calculateCountBottles(age){
      var calculate = age * 150;
      return calculate;
    }

    function calculateAge(dateOfBirth){
      var birth = new Date(dateOfBirth);
      var today = new Date();
      var diff = today - birth;
      var age = Math.floor(diff / (31536000000));
      return age;
    }

    function setCurrentCountDownDay(interval){
      var countDown = getDiffDays();
      $interval.cancel(interval);
      return countDown;
    }

    function setCurrentCountDownHour(interval){
        var countDown = getDiffHours();
        $interval.cancel(interval);
        return countDown;
    }

    function setCurrentCountDownMinute(interval){
      var countDown = getDiffMinutes();
      $interval.cancel(interval);
      return countDown;
    }

    function setCurrentCountDownTrash(interval){
      var countDown = getCountTruckTrashes();
      $interval.cancel(interval);
      return countDown;
    }

    function setUser(userName){
      $window.localStorage.setItem(env.tagUser, userName);
      vm.userName = $window.localStorage.getItem(env.tagUser);
    }

    function setBirthDate(date){
      $window.localStorage.setItem(env.filterName, date);
    }

    function getCountOfPlasticForSecondMessage(){
      var age = calculateAge(getBirthDate());
      return calculateCountPlastic(age);
    }

    function getUser(){
      return vm.userName;
    }
  }
  
})();
