(function(){
  'use strict'
  
  angular
    .module('marvivaApp.main')
    .config(setupRoutes);


  setupRoutes.$inject = ['$stateProvider'];
  function setupRoutes($stateProvider){
    $stateProvider
      .state('main', {
        params: {
            name: null,
            date: null
        },
        templateUrl: 'components/main/view.html',
        controller: 'MainCtrl as vm'
      });
  }

})();