(function(){
  'use strict'
  
  angular
    .module('marvivaApp.register')
    .controller('RegisterCtrl',RegisterCtrl);

  /* @ngInject */
  function RegisterCtrl(RegisterSrv, $state, env, $filter) {

    var vm = this;
    vm.toggleCalendarPopup=toggleCalendarPopup;
    vm.registerUser = registerUser;
    vm.newUser={};
    vm.meh={};

    vm.dateOptions= {
        showWeeks: false,
        datepickerMode:"year"
    };

    vm.calendarPopup = {
        opened: false
    };

    function registerUser(){
      vm.newUser.birthdate = $filter(env.filterName)(vm.newUser.date, env.formatDate);
      RegisterSrv.registerUser(vm.newUser);
      $state.go(env.mainView,{
        name: vm.newUser.name,
        date: vm.newUser.birthdate
      });
    }

    function toggleCalendarPopup() {
      vm.calendarPopup.opened = true;
      return vm.calendarPopup.opened;
    }
  }

})();
