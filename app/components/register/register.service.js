(function(){
  'use strict'
  
  angular
    .module('marvivaApp.register')
    .service('RegisterSrv',RegisterSrv);

  function RegisterSrv(){
    var vm = this;
    var rootRef = firebase.database().ref();
    var users = rootRef.child("users");
    vm.registerUser = registerUser;

    function registerUser(data){
      users.push(data);
    }
  }
  
})();