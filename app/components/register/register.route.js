(function(){
  'use strict'
  
  angular
    .module('marvivaApp.register')
    .config(setupRoutes);


  setupRoutes.$inject = ['$stateProvider'];
  function setupRoutes($stateProvider){
    $stateProvider
      .state('register', {
        url: '/user',
        templateUrl: 'components/register/view.html',
        controller: 'RegisterCtrl as vm'
      });
  }
  
})();