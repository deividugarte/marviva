(function(){
  'use strict';
  angular
    .module('marvivaApp')
    .config(setRoutes);

  setRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];
  function setRoutes($stateProvider, $urlRouterProvider){
    $urlRouterProvider.otherwise('/user');

    $stateProvider
      .state('home', {
        url: '/user',
        templateUrl: 'components/register/view.html'
      })
  }
})();
