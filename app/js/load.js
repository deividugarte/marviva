$(document).ready(function(){
    getCurrentBrowser();
});

function getCurrentBrowser(){

    if(navigator.userAgent.indexOf("Chrome") != -1 ){
        $('head').append("<link rel='stylesheet' href='css/first-message.css'>");
        $('head').append("<link rel='stylesheet' href='css/second-message.css'>");
        $('head').append("<link rel='stylesheet' href='css/fourth-message.css'>");
        $('head').append("<link rel='stylesheet' href='css/eighth-message.css'>");
    }
    else if(navigator.userAgent.indexOf("Firefox") != -1 ){
        $('head').append("<link rel='stylesheet' href='css/firefox-first-message.css'>");
        $('head').append("<link rel='stylesheet' href='css/firefox-second-message.css'>");
        $('head').append("<link rel='stylesheet' href='css/firefox-eighth-message.css'>");
        $('head').append("<link rel='stylesheet' href='css/fourth-message.css'>");
    }
    else if((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )){ //IF IE > 10{
        $('head').append("<link rel='stylesheet' href='css/ie-first-message.css'>");
        $('head').append("<link rel='stylesheet' href='css/second-message.css'>");
        $('head').append("<link rel='stylesheet' href='css/ie-fourth-message.css'>");
        $('head').append("<link rel='stylesheet' href='css/ie-eighth-message.css'>");
    }
    else{
        $('head').append("<link rel='stylesheet' href='css/first-message.css'>");
        $('head').append("<link rel='stylesheet' href='css/second-message.css'>");
        $('head').append("<link rel='stylesheet' href='css/fourth-message.css'>");
        $('head').append("<link rel='stylesheet' href='css/eighth-message.css'>");
    }
}