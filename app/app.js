(function(){
'use strict'
  angular.module('marvivaApp',[
    'marvivaApp.common',
    'marvivaApp.register',
    'marvivaApp.main',
    'marvivaApp.final'
  ]);
})();
