(function(){
  'use strict';
  angular
    .module('marvivaApp')
    .run(run);

  run.$inject = ['$window']
  function run($window){
    var config =
    {
      apiKey: "AIzaSyCE0AoNradl_j4UZpRQIfugbU3UGuXKg5w",
      authDomain: "marviva-e8e9d.firebaseapp.com",
      databaseURL: "https://marviva-e8e9d.firebaseio.com",
      storageBucket: "marviva-e8e9d.appspot.com",
    };
    window.firebase.initializeApp(config);
  }
})();
